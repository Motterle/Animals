import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
import "./jestsetup"
import {I18nextProvider} from "react-i18next";
import i18n from '../src/i18n';


import {Box} from "../src/components/Box";




test('App renders correctly', () => {

    const wrapper = mount(<I18nextProvider i18n={i18n}><Box t={key=>key} name={"tiger"}/></I18nextProvider>);
    //const wrapper = mount(<App t={key=>key}/>);
    console.log(wrapper.debug());
    expect(wrapper.find("p").length).toBe(1);
});

test('App renders correctly', () => {

    const wrapper = mount(<I18nextProvider i18n={i18n}><App/></I18nextProvider>);
    //const wrapper = mount(<App t={key=>key}/>);
    expect(wrapper.find("App").length).toBe(1);
});


test('render a app', () => {
    const wrapper = shallow(<App/>);
    expect(wrapper).toMatchSnapshot();
});

test('render a small app', () => {
    const wrapper = shallow(
        <App small>Hello Jest!</App>
    );
    expect(wrapper).toMatchSnapshot();
});

test('render a grayish app', () => {
    const wrapper = shallow(
        <App light>Hello Jest!</App>
    );
    expect(wrapper).toMatchSnapshot();
});

test('render a app title', () => {
    const wrapper = shallow(
        <App title="User" />
    );
    expect(wrapper.prop('title')).toEqual('User');
});
