import React, { Component } from 'react';
import uuid from "uuid/v4";
import Box from "./components/Box.js";
import MainPage from "./components/MainPage";
import HelloPage from "./components/HelloPage";
import {translate, Trans} from "react-i18next";
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";

export class App extends Component {

  constructor(){
    super();
    this.state = {
      names:[
        "tiger","elephant","eagle","leopard","shark","gecko"
      ]
    };
  }

  render() {
    const {t, i18n} = this.props;
    const changeLanguage = lng => {
            i18n.changeLanguage(lng);
    }
//istruzioni mettete /user per vedere la pagina della fattoria
    return (
        <Router>
            <Switch>

                <Route
                    path="/user"
                    render={(routeProps) =>
                        <MainPage
                            {...routeProps}>
                        </MainPage>}
                />
                <Route
                    path="/"
                    render={(routeProps) =>
                        <HelloPage
                            {...routeProps}
                            >
                        </HelloPage>}
                />

            </Switch>
        </Router>
    );
  }
}

export default translate("translations")(App);