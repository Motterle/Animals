import React, { Component } from 'react';
import uuid from "uuid/v4";

import {translate, Trans} from "react-i18next";

export class HelloPage extends Component {

    constructor(){
        super();
        this.state = {
            names:[
                "tiger","elephant","eagle","leopard","shark","gecko"
            ]
        };
    }

    render() {
        const {t, i18n} = this.props;
        const changeLanguage = lng => {
            i18n.changeLanguage(lng);
        }

        return (
            <div style={{margin:"1em"}}>
                <h1 style={{textAlign:"center"}}><Trans>Lo sapevi che I18next fa schifo??</Trans></h1>

            </div>
        );
    }
}

export default translate("translations")(HelloPage);