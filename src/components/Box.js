import React, { Component } from 'react';
import {translate, Trans} from "react-i18next";

export class Box extends Component {
  render() {
  	const {t, i18n} = this.props;
    const changeLanguage = lng => {
            i18n.changeLanguage(lng);
    }
    
    return (
      <div className="App" 
      style={{backgroundColor:"#ffb38b",maxWidth:"max-content",padding:"1em",
			  margin:"1em",float:"left"}}>
      <p style={{color:"#4e7d9a"}}>{t(this.props.name)}</p>
      </div>
    );
  }
}

export default translate("translations")(Box);