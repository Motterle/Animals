import React, { Component } from 'react';
import uuid from "uuid/v4";
import Box from "../components/Box.js";
import {translate, Trans} from "react-i18next";

export class MainPage extends Component {

    constructor(){
        super();
        this.state = {
            names:[
                "tiger","elephant","eagle","leopard","shark","gecko"
            ]
        };
    }

    render() {
        const {t, i18n} = this.props;
        const changeLanguage = lng => {
            i18n.changeLanguage(lng);
        }

        return (
            <div style={{margin:"1em"}}>
                <h1 style={{textAlign:"center"}}><Trans>Name of some animals</Trans></h1>
                <div className="App" style={{backgroundColor:"#8dbcda",overflow:"auto",padding:"1em"}}>
                    {this.state.names.map((name) => <Box key={uuid()} name={name}/>)}
                </div>
            </div>
        );
    }
}

export default translate("translations")(MainPage);