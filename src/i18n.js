import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import box_en from "./i18n/en/box.json";
import box_it from "./i18n/it/box.json";
import app_en from "./i18n/en/app.json";
import app_it from "./i18n/it/app.json";
i18n.use(LanguageDetector).init({
    resources:{
        en: {
        	App: app_en,
            Box: box_en,
            },
        it: {
        	App:app_it,
            Box: box_it,
        }
        },
    fallbackLng: "en",
    debug: true,

    keySeparator: false,

    interpolation: {
        escapeValue: false,
        formatSeparator: ","
    },

    react: {
        wait: true
    }
});

export default i18n;